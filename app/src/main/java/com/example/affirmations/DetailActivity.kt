package com.example.affirmations

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Phone
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.AffirmationsTheme
import com.example.affirmations.ui.viewmodel.DetailViewModel
import androidx.compose.foundation.Image
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource



class DetailActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val id = intent.getIntExtra("id", 0)
        setContent {
            DetailInformation(index = id)
        }
    }
}

@Composable
fun DetailInformation(index: Int, detailViewModel: DetailViewModel = viewModel()) {
    detailViewModel.getAffirmation(index)
    val uiState by detailViewModel.uiState.collectAsState()
    uiState?.let {
        AffirmationCardActivity(it)
    }
}


@Composable
fun AffirmationCardActivity(uiState: Affirmation) {
    AffirmationsTheme {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(color = MaterialTheme.colors.background),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally,

            ) {
            AffirmationCardView(
                affirmation = uiState,
                )
        }
    }

}

@Composable
fun AffirmationCardView(affirmation: Affirmation, modifier: Modifier = Modifier) {
    Image(
        painter = painterResource(id = affirmation.imageResourceId),
        contentDescription = stringResource(id = affirmation.Desc),
        modifier = Modifier
            .height(200.dp)
            .fillMaxWidth(),
        contentScale = ContentScale.Crop
    )
    Description(affirmation)

}


@Composable
fun Description(affirmation: Affirmation) {

    Text(
        text = "Description: ",
        color = MaterialTheme.colors.primary,
        modifier = Modifier
            .padding(2.dp)
    )

    Text(
        text = stringResource(id = affirmation.stringDescriptionID),
        color = MaterialTheme.colors.primaryVariant,
        modifier = Modifier
            .padding(start = 15.dp, end = 15.dp),
        textAlign = TextAlign.Justify
    )
    Spacer(modifier = Modifier.height(10.dp))
    MailPhoneIcons(affirmation)

}


@Composable
fun MailPhoneIcons(affirmation: Affirmation) {

    Row(modifier = Modifier
        .padding(10.dp)
        .fillMaxWidth(), horizontalArrangement = Arrangement.End, verticalAlignment = Alignment.Bottom) {
        val context = LocalContext.current
        Button(onClick = {
            context.sendMail(
                to=context.getString(affirmation.Mail), subject = context.getString(affirmation.Desc))
        }) {
            Icon(
                imageVector = androidx.compose.material.icons.Icons.Filled.Email,
                contentDescription = "Mail"
            )
        }
        Spacer(modifier = Modifier.width(10.dp))
        Button(onClick = {
            context.dial(phone = context.getString(affirmation.Telephone))
        }) {
            Icon(
                imageVector = androidx.compose.material.icons.Icons.Filled.Phone,
                contentDescription = "Phone"
            )
        }
    }
}

private fun Context.sendMail(to: String, subject: String) {
    try {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        startActivity(Intent.createChooser(intent,"Choose Mail: "))
    } catch (e: ActivityNotFoundException) {
        Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show()
    } catch (t: Throwable) {
        Toast.makeText(this, t.toString(), Toast.LENGTH_LONG).show()
    }
}

private fun Context.dial(phone: String) {
    try {
        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
        startActivity(intent)
    } catch (t: Throwable) {
        Toast.makeText(this, t.toString(), Toast.LENGTH_LONG).show()
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    AffirmationsTheme {
        AffirmationCardActivity(Datasource().loadAffirmations().last())
    }
}
