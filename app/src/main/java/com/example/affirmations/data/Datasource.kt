/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations.data
import com.example.affirmations.R
import com.example.affirmations.model.Affirmation
/**
 * [Datasource] generates a list of [Affirmation]
 */
class Datasource() {
    fun loadAffirmations(): List<Affirmation> {
        return listOf<Affirmation>(
            Affirmation(R.string.affirmation1, R.drawable.image1,R.string.description1,1, R.string.desc1, R.string.mail1, R.string.phone1),
            Affirmation(R.string.affirmation2, R.drawable.image2,R.string.description2,2, R.string.desc2, R.string.mail2, R.string.phone2),
            Affirmation(R.string.affirmation3, R.drawable.image3,R.string.description3,3, R.string.desc3, R.string.mail3, R.string.phone3),
            Affirmation(R.string.affirmation4, R.drawable.image4,R.string.description4,4, R.string.desc4, R.string.mail4, R.string.phone4),
            Affirmation(R.string.affirmation5, R.drawable.image5,R.string.description5,5, R.string.desc5, R.string.mail5, R.string.phone5),
            Affirmation(R.string.affirmation6, R.drawable.image6,R.string.description6,6, R.string.desc6, R.string.mail6, R.string.phone6),
            Affirmation(R.string.affirmation7, R.drawable.image7,R.string.description7,7, R.string.desc7, R.string.mail7, R.string.phone7),
            Affirmation(R.string.affirmation8, R.drawable.image8,R.string.description8,8, R.string.desc8, R.string.mail8, R.string.phone8),
            Affirmation(R.string.affirmation9, R.drawable.image9,R.string.description9,9, R.string.desc9, R.string.mail9, R.string.phone9),
            Affirmation(R.string.affirmation10, R.drawable.image10,R.string.description10,10, R.string.desc10, R.string.mail10, R.string.phone10))

    }
}
