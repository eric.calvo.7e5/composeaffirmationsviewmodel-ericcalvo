/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations


import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.AffirmationsTheme
import com.example.affirmations.ui.viewmodel.MainViewModel
import androidx.lifecycle.viewmodel.compose.viewModel

class MainActivity : ComponentActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContent {
      // TODO 5. Show screen
        AffirmationApp()
    }
  }
}

@Composable
fun AffirmationApp(mainViewModel: MainViewModel = viewModel()) {
  // TODO 4. Apply Theme and affirmation list
    AffirmationsTheme {
      val affirmationUiState by mainViewModel.uiState.collectAsState()
      AffirmationList(affirmationList = affirmationUiState,
      modifier = Modifier.background(color = MaterialTheme.colors.background))
  }
}

@Composable
fun AffirmationList(affirmationList: List<Affirmation>, modifier: Modifier = Modifier) {
  // TODO 3. Wrap affirmation card in a lazy column
    LazyColumn {
      items(affirmationList) { affirmation ->
        AffirmationCard(affirmation, modifier = Modifier.padding(all = 4.dp))
    }
  }
}

@Composable
fun AffirmationCard(affirmation: Affirmation, modifier: Modifier = Modifier) {
  // TODO 1. Your card UI
  var expanded by remember { mutableStateOf(false) }
  val color by animateColorAsState(
    targetValue = if (expanded) colors.background else colors.background,
  )

  Card(
    modifier = Modifier
      .fillMaxSize()
      .padding(vertical = 5.dp, horizontal = 10.dp),
    border =
    BorderStroke(
            3.dp, color = colors.primary
        ),
    elevation = 5.dp

  ) {
    Column(
      modifier = Modifier
        .animateContentSize(
          animationSpec = spring(
            dampingRatio = Spring.DampingRatioMediumBouncy,
            stiffness = Spring.StiffnessVeryLow
          )
        )
        .background(color = color)
    ) {
      Row {
        Image(
          painter = painterResource(id = affirmation.imageResourceId),
          contentDescription = "imagen",
          modifier = Modifier
            .padding(12.dp)
            .size(64.dp)
            .clip(RoundedCornerShape(20)),
          contentScale = ContentScale.Crop
        )
        Text(
          text = stringResource(id = affirmation.stringResourceId),
          textAlign = TextAlign.Left,
          fontSize = 18.sp,
          modifier = Modifier
            .padding(top = 16.dp, end = 16.dp, bottom = 16.dp, start = 8.dp)
            .weight(1f)
        )
        AffirmationCardButton(
          expanded = expanded,
          onClick = {
            expanded = !expanded
          }
        )
      }
      if (expanded) {
        Description(affirmation = affirmation, OnClick = {expanded = !expanded})
      }
    }
  }
}

@Composable
fun AffirmationCardButton(
  expanded: Boolean,
  onClick: () -> Unit,
  modifier: Modifier = Modifier
) {
  IconButton(onClick = onClick) {
    Icon(
      imageVector = if (expanded) Icons.Filled.ExpandLess else Icons.Filled.ExpandMore,
      tint = colors.secondary,
      contentDescription = "", modifier = Modifier.size(30.dp)
    )
  }

}

@Composable
fun Description(affirmation: Affirmation, OnClick: () -> Unit) {
  Column(
    modifier = Modifier.padding(
      start = 8.dp,
      top = 4.dp,
      bottom = 8.dp,
      end = 4.dp
    )
  ) {
    Text(
      text = "Description: ",
      color = colors.primary,
      modifier = Modifier
        .padding(2.dp)
    )

    Text(
      text = stringResource(id = affirmation.stringDescriptionID),
      modifier = Modifier
        .padding(2.dp),
      overflow = TextOverflow.Ellipsis,
      maxLines = 2
    )
    Row(
      modifier = Modifier
        .padding(end = 4.dp)
    ) {
      Spacer(modifier = Modifier.weight(4f))
      val mContext = LocalContext.current
      Button(onClick = {
        val intent = Intent(mContext, DetailActivity::class.java)
        intent.putExtra("id", affirmation.id)
        mContext.startActivity(intent)
      }
      ) {

        Icon(
          imageVector = Icons.Filled.Add,
          tint = colors.secondary,
          contentDescription = "Ver Más"
        )
        Text(
          text = "See More",
          color = colors.surface,
          modifier = Modifier
            .padding(start = 1.dp)

        )

      }
    }


  }
}


@Preview
@Composable
private fun AffirmationCardPreview() {
  // TODO 2. Preview your card
  AffirmationCard (        affirmation = Affirmation(
    R.string.affirmation1,
    R.drawable.image1,
    R.string.description1,
    1
  )
  )
}
