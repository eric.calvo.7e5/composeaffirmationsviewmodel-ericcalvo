package com.example.affirmations.ui.viewmodel


import androidx.lifecycle.ViewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class DetailViewModel: ViewModel() {

    private val _uiState = MutableStateFlow<Affirmation?>(null)
    val uiState: StateFlow<Affirmation?> = _uiState.asStateFlow()


    fun getAffirmation(cardID: Int) {
        val afirmationList=Datasource().loadAffirmations()
        _uiState.value= afirmationList.firstOrNull { it.id==cardID }
    }
}
